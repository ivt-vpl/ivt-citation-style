This is a special citation style used by the Institute of Transport Systems and Planning at ETH Zurich.

Currently, this repository contains a Zotero Stylesheet (.csl). Other formats may be added in the future.

The original style guide may be found here: https://www.ivt.ethz.ch/studium/downloads.html